+ Env {

	*lfo { |rate = 1, lo = -1, hi = 1, duration = 1, offset = 0, curve|
		var levels, times, mul, add, now, inc;
		this.new.checkRate(rate, duration);
		now = 0.0;
		inc = 0;
		levels = [];
		times = [];
		#lo, hi, rate = this.new.checkInputs(lo, hi, rate, duration);
		while({ now <= duration }, {
			mul = (hi[now] - lo[now]) * 0.5;
			add = mul + lo[now];
			levels = levels.add(-1**(inc) * mul + add);
			times = times.add(rate[now].reciprocal * 0.5);
			inc = inc + 1;
			now = now + (rate[now].reciprocal * 0.5);
		});
		curve = curve ? 'sin';
		(levels.size == 1).if({levels = levels.dup(2).flatten});

		^Env(levels, times, curve, offset: offset)
	}

	*sinLFO { |rate = 1, lo = -1, hi = 1, duration = 1, offset = 0|
		^Env.lfo(rate, lo, hi, duration, offset, 'sin')
	}

	*trianlgeLFO { |rate = 1, lo = -1, hi = 1, duration = 1, offset = 0|
		^Env.lfo(rate, lo, hi, duration, offset, 'lin')
	}

	*squareLFO { |rate = 1, lo = -1, hi = 1, duration = 1, offset = 0|
		^Env.lfo(rate, lo, hi, duration, offset, 'step')
	}

	*sawLFO { |rate = 1, lo = -1, hi = 1, duration = 1, offset = 0, curve|
		var levels, times, mul, add, now, inc;
		this.new.checkRate(rate, duration);
		now = 0.0;
		inc = 0;
		levels = [];
		times = [];
		#lo, hi, rate = this.new.checkInputs(lo, hi, rate, duration);
		while({ now <= duration }, {
			mul = (hi[now] - lo[now]) * 0.5;
			add = mul + lo[now];
			levels = levels.add(-1**(inc) * mul + add);
			times = times.add([rate[now].reciprocal, 0.0][inc.mod(2)]);
			inc = inc + 1;
			now = now + (rate[now].reciprocal * 0.5);

		});
		curve = curve ? 'lin';
		(levels.size == 1).if({levels = levels.dup(2).flatten});

		^Env(levels, times, curve, offset: offset)
	}

	*isawLFO { |rate = 1, lo = -1, hi = 1, duration = 1, offset = 0, curve|
		var levels, times, mul, add, now, inc;
		this.new.checkRate(rate, duration);
		now = 0.0;
		inc = 0;
		levels = [];
		times = [];
		#lo, hi, rate = this.new.checkInputs(lo, hi, rate, duration);
		while({ now <= duration }, {
			mul = (hi[now] - lo[now]) * 0.5;
			add = mul + lo[now];
			levels = levels.add(-1**(inc) * mul + add);
			times = times.add([0.0, rate[now].reciprocal][inc.mod(2)]);
			inc = inc + 1;
			now = now + (rate[now].reciprocal * 0.5);

		});
		curve = curve ? 'lin';
		(levels.size == 1).if({levels = levels.dup(2).flatten});

		^Env(levels, times, curve, offset: offset)
	}

	*noiseLFO { |rate = 1, lo = -1, hi = 1, duration = 1, mode = 'osc', offset = 0, curve = 'sin'|
		var levels, times, mul, add, now, inc;
		this.new.checkRate(rate, duration);
		now = 0.0;
		inc = 0;
		levels = [];
		times = [];
		#lo, hi, rate = this.new.checkInputs(lo, hi, rate, duration);
		while({ now <= duration }, {
			mul = (hi[now] - lo[now]) * 0.5;
			add = mul + lo[now];
			(mode == 'osc').if({
				levels = levels.add([-1.0.rrand(0.0), 0.0.rrand(1.0)][inc.mod(2)] * mul + add);
			}, {
				levels = levels.add(1.0.rand2 * mul + add);
			});
			times = times.add(rate[now].reciprocal * 0.5);
			inc = inc + 1;
			now = now + (rate[now].reciprocal * 0.5);

		});

		(levels.size == 1).if({levels = levels.dup(2).flatten});

		^Env(levels, times, curve, offset: offset)
	}

	checkRate { |rate, duration|
		rate.isKindOf(Env).if({ rate = rate.levels.sum/rate.levels.size });
		((rate.reciprocal * 0.5) > duration).if({
			"Rate is too slow for the given duration. A straight line will be returned".warn
		})
	}

	reciprocal {
		this.levels = this.levels.reciprocal
	}

	scale { |levelScalar = 1, timeScalar = 1|
		this.levels = this.levels * levelScalar;
		this.times = this.times * timeScalar
	}

	levelScale { |levelScalar = 1|
		this.scale(levelScalar, 1.0)
	}

	derivative { |time, step = (48000.reciprocal)|
		(time >= (this.duration + this.offset)).if({ step = step.neg });
		^((this[time + step] - this[time])/step)
	}

	derivativeEnv { |step = 0.01|
		var duration = this.duration;
		var levels, times;
		var now = 0.0;
		while({ now <= duration}, {
			levels = levels.add(this.derivative(now));
			times = times.add(now);
			now = now + step
		});
		^Env.pairs([times, levels].flop, 'sin')
	}

	timeScale { |timeScalar = 1|
		this.scale(1.0, timeScalar)
	}

	+ { |num|
		num.isKindOf(Env).if(num = num.levels);
		^Env(this.levels + num, this.times, this.curves, offset: this.offset)
	}

	resample { |length, normalizeTime = false|
		var levels, times;
		length = length ?? { this.levels.size };
		levels = length.collect{ |now| this[now/length * this.duration] };
		times = 1.dup(length - 1).normalizeSum * normalizeTime.if({ this.duration }, { 1 });
		^Env.new(levels, times)
	}

	midiratio { |shiftFactor = 0|
		this.levels = (this.levels + shiftFactor).midiratio
	}

	midicps { |shiftFactor = 0|
		this.levels = (this.levels + shiftFactor).midicps
	}

	linlin { |inMin, inMax, outMin, outMax, clip = \minmax|
		this.levels = this.levels.linlin(inMin, inMax, outMin, outMax, clip)
	}

	atLevel { |level|
		var begLevel, endLevel, segdur, begTime, times, result = [], op, curve;
		// actual begining time needed for calculations is 0
		times = [0] ++ this.times;
		// iterate over each segment represented by the times array
		this.times.do{ |thisTime, i|
			// get the curve for current segment
			curve = this.curves.isKindOf(Array).if({this.curves[i]}, {this.curves});
			// set the math operator based on the curve
			op = (curve ++ 'Pos').asSymbol;
			// the beginning time of the segment
			begTime = times[0..i].sum + this.offset;
			// the segment duration
			segdur = thisTime;
			// set beginning and end levels of segment
			begLevel = this.levels[i];
			endLevel = this.levels[i+1];

			// check if level is in the segment
			(level.inclusivelyBetween(begLevel, endLevel) or: level.inclusivelyBetween(endLevel, begLevel)).if({
				// if not a flat segment nor a zero time sgement
				((begLevel == endLevel) or: (thisTime == 0.0)).not.if({
					// do the math to find the time of the level and add to array
					result = result.add(this.perform(op, level, begLevel, endLevel, segdur, begTime));
				}, {
					// if flat seg or zero time
					result = result.add(begTime)
				})
			})
		};
		^result
	}

	sinPos { |level, begLevel, endLevel, segdur, begTime|
		^(((acos((((level - begLevel)/(endLevel - begLevel)) - 0.5)/ -1 / 0.5)/pi) * segdur) + begTime)
	}

	linPos { |level, begLevel, endLevel, segdur, begTime|
		^((((level - begLevel) / (endLevel - begLevel)) * segdur) + begTime)
	}

	expPos { |level, begLevel, endLevel, segdur, begTime|
		^(((log(level / begLevel) / log(endLevel / begLevel)) * segdur) + begTime)
	}

	pad { |totalDuration|
		this.levels = this.levels ++ this.levels.last;
		this.times = this.times ++ (totalDuration - this.totalDuration)
	}

	copyRange { |first, last|
		var step = 0.01, levels, times, curves, now, firstIndex, lastIndex;

		firstIndex = this.times.integrate.indexOfGreaterThan(first);
		lastIndex = this.times.integrate.indexOfGreaterThan(last);
		firstIndex.isNil.if({ firstIndex = this.times.size });
		lastIndex.isNil.if({ lastIndex = this.times.size });
		levels = levels.add(this[first]);
		times = times.add(0.0);
		curves = this.curves.isKindOf(Array).if({ [this.curves[firstIndex]] }, { this.curves });


		while({ firstIndex < lastIndex }, {
			levels = levels.add(this.levels[firstIndex + 1]);
			times = times.add(this.times.integrate[firstIndex] - first);
			curves.isKindOf(Array).if({
				curves = curves.add(this.curves[firstIndex])
			});
			firstIndex = firstIndex + 1;
		});
		levels = levels.add(this[last]);
		times = times.add(last - first);
		^Env.pairs([times, levels].flop, curves)
	}

	copySeries { |first, second, last|
		^this.copyRange(first, last)
	}

	checkInputs { |lo, hi, rate, duration|
		var rateIsNeg = false;
		rate.isKindOf(Env).if({ (rate.levels.minItem < 0).if({ rateIsNeg = true }) }, { rateIsNeg = rate.isNegative });
		rateIsNeg.if({ "Rate is negative, using the absolute value".warn });
		^[
			lo.isKindOf(Env).not.if({ Env([lo, lo], [duration]) }, { Env(lo.levels, lo.times, lo.curves) }),
			hi.isKindOf(Env).not.if({ Env([hi, hi], [duration]) }, { Env(hi.levels, hi.times, hi.curves) }),
			rate.isKindOf(Env).not.if({ Env([rate, rate].abs, [duration]) }, { Env(rate.levels.abs, rate.times, rate.curves) })
		]
	}

}







