Degrees {
	var <tuning, numNotes, <startDegree;
	var <>hz, <>midiRatios, ratios, <>midiNotes, tuningSize, numOctaves;

	*new { |tuningOrScale, numNotes, startDegree|
		^super.newCopyArgs(tuningOrScale, numNotes, startDegree).init
	}

	*goldenStars { |numNotes, startDegree|
		var notes;
		notes = Array.geom(numNotes, this.goldenRatio**(-8) * 1000, this.goldenRatio**(2/7)).cpsmidi;
		notes = notes - notes.first + startDegree;
		^super.new.midiNotes_(notes).midiRatios_(notes.midiratio).hz_(notes.midicps)
	}

	init {
		ratios = tuning.ratios;
		tuningSize = ratios.size.asInteger;
		numOctaves = (numNotes/tuningSize).asInteger;
		midiRatios = numNotes.collect{ |i|
			ratios.wrapAt(i) * (tuning.octaveRatio**((i/tuningSize).floor))
		};
		midiNotes = midiRatios.ratiomidi + startDegree;
		hz = midiNotes.midicps;
	}

	goldenRatio {
		^(1 + 5.sqrt)/2
	}


}

TuningDegrees {
	var <tuning, numNotes, <startDegree;
	var <>hz, <>midiRatios, ratios, <>midiNotes, tuningSize, numOctaves;

	*new { |tuning, numNotes, startDegree|
		^super.newCopyArgs(tuning, numNotes, startDegree).init
	}

	*goldenStars { |numNotes, startDegree|
		var notes;
		notes = Array.geom(numNotes, this.goldenRatio**(-8) * 1000, this.goldenRatio**(2/7)).cpsmidi;
		notes = notes - notes.first + startDegree;
		^super.new.midiNotes_(notes).midiRatios_(notes.midiratio).hz_(notes.midicps)
	}

	init {
		ratios = tuning.ratios;
		tuningSize = ratios.size.asInteger;
		numOctaves = (numNotes/tuningSize).asInteger;
		midiRatios = numNotes.collect{ |i|
			ratios.wrapAt(i) * (tuning.octaveRatio**((i/tuningSize).floor))
		};
		midiNotes = midiRatios.ratiomidi + startDegree;
		hz = midiNotes.midicps;
	}

	goldenRatio {
		^(1 + 5.sqrt)/2
	}


}

ScaleDegrees {
	var scale, tuningDegree;
	var <tuning, scaleSize, <midiNotes, <hz, <midiRatios, numNotes, degrees, numOctaves;
	var tuningSize, ratios;

	*new { |scale, tuningDegree|
		^super.newCopyArgs(scale, tuningDegree).init
	}

	init {
		midiNotes = tuningDegree.midiNotes;
		numNotes = midiNotes.size;
		tuning = tuningDegree.tuning;
		tuningSize = tuning.size;
		degrees = scale.degrees;
		numOctaves = (numNotes/tuningSize).ceil.asInt;
		midiNotes = midiNotes[numOctaves.collect{ |i|
			degrees + (tuningSize * i)
		}.flatten.select{ |item| item <= (numNotes-1)}];
		hz = midiNotes.midicps;
		midiRatios = (midiNotes - midiNotes.first).midiratio
	}


}
