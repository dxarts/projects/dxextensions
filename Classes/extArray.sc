+ Array {

	removeDuplicates {
		var newArray = [];
		this.do{ |item|
			newArray.includes(item).not.if({ newArray = newArray.add(item) });
		};
		^newArray
	}

}