+ Cartesian {

	*rand { |lo = -1.0, high = 1.0|
		^Cartesian.new(lo.rrand(high), lo.rrand(high), lo.rrand(high))
	}

	clip { |lo, hi|
		this.x_(this.x.clip(lo, hi));
		this.y_(this.y.clip(lo, hi));
		this.z_(this.z.clip(lo, hi));
	}

	translateQuadrant { |quadrant = 1, max = 1|
		var cart;
		cart = quadrant.switch(
			1, {Cartesian(0.5, 0.5, 0.5)},
			2, {Cartesian(-0.5, 0.5, 0.5)},
			3, {Cartesian(-0.5, -0.5, 0.5)},
			4, {Cartesian(0.5, -0.5, 0.5)}
		);
		cart = cart * max;
		^this.scale(cart).translate(cart)
	}

	normalize {
		^(this.rho > 0).if({ this/this.rho }, { this })
	}

	limit { |max|
		(this.rho > max).if({
			^(this.normalize * max)
		})
	}

	linlin { |lo, hi, lo1, hi1, clip|
		^Cartesian.new(this.x.linlin(lo, hi, lo1, hi1, clip),
			this.y.linlin(lo, hi, lo1, hi1, clip),
			this.z.linlin(lo, hi, lo1, hi1, clip));
	}

}

+ Point {

	normalize {
		^(this.rho > 0).if({ this/this.rho }, { this })
	}

	limit { |max|
		(this.rho > max).if({
			^(this.normalize * max)
		})
	}
}