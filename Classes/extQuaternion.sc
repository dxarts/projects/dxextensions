+ Quaternion {

	// // roll
	// tilt {
	// 	^atan2((2 * (a * b - (c * d))), (1 - (2*(b.squared + c.squared))))
	// }
	//
	// // pitch
	// tumble {
	// 	^asin((2 * (a * c + (b * d))).clip(-1.0, 1.0));
	// }
	//
	// // yaw
	// rotate {
	// 	^atan2((2 * (a * d - (c * b))), (1 - (2*(d.squared + c.squared))))
	// }
	//
	// // roll
	// tilt {
	// 	^atan2((2 * (a * b + (c * d))), (1 - (2*(b.squared + c.squared)))).neg.wrap(-pi, pi)
	// }
	//
	// // pitch
	// tumble {
	// 	var z, y, gz, ang, val;
	// 	val = (2 * (a * c - (b * d))).clip(-1.0, 1.0);
	// 	ang = asin(val);
	// 	// select which axis is our reference for tumble (y or z) (used to determine the quadrant)
	//
	// 	// calc z and y
	// 	z = (a.squared - b.squared - c.squared + d.squared);
	// 	y = (2 * (a * b + (c * d)));
	//
	// 	// choose which axis is more oriented toward gravity
	// 	gz = [z, y][[z, y].abs.maxIndex];
	//
	// 	// if the reference is negative, move to quadrants 2 and 3
	// 	^if(gz.isNegative) { pi - ang } { ang }.wrap(-pi, pi)
	// }
	//
	// // yaw
	// rotate {
	// 	var x, z;
	// 	x = (2 * (a * d + (c * b)));
	// 	z = (1 - (2*(d.squared + c.squared)));
	// 	((this.tumble > 0.5pi) or: (this.tumble < -0.5pi)).if({ z = z.neg });
	// 	^atan2(x, z)
	// }

	set {arg newA, newB, newC, newD;
		#a, b, c, d = [newA, newB, newC, newD];
	}

	vector {
		^Cartesian.new(
			2 * ((b * d) - (a * c)),
			2 * ((c * d) - (a * b)),
			1 - (2 * (b.squared + c.squared))
		)
	}

	toArray {
		^[a, b, c, d]
	}

}