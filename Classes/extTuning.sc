+ Tuning {

	*chowning { |numDivisions|
		^Tuning.new(numDivisions.collect{|i| i.linlin(0, 11, 0, 9)}, 0.goldenRatio, 'Chowning')
	}

	expandDegrees { |numNotes, startDegree, scale|
		var degrees, size, tuningNotes;
		size = this.size;
		tuningNotes = scale.notNil.if({ (numNotes/scale.size * tuning.size + 1).asInteger }, { numNotes });
		degrees = tuningNotes.collect{ |i|
			this.semitones.wrapAt(i) + (12 * (i/size).floor)
		} + startDegree;
		scale.notNil.if({
			degrees = degrees[
				numNotes.collect{ |index|
					scale.degrees.wrapAt(index) + ((index/scale.size).floor * size)
				}.flatten;
			]
		});
		^degrees
	}

}