+ SphericalDesign {

	oppositeIndex { |index|
		^this.vectorAngles(*this.directions[index]).maxIndex
	}

	oppositePoint { |index|
		^this.points.at(this.oppositeIndex(index))
	}

	oppositePairsIndices { |theta = 0, phi = 0|
		var newIndices;
		newIndices = this.nearestIndicesWithin(theta, phi, pi)[0..this.size-1];
		^newIndices.collect{ |thisIndex|
			[thisIndex, this.oppositeIndex(thisIndex)]
		}.flatten
	}

	oppositePairsIndicesRev { |theta = 0, phi = 0|
		var newIndices;
		newIndices = this.nearestIndicesWithin(theta, phi, pi)[0..this.size-1];
		^newIndices.collect{ |thisIndex|
			[this.oppositeIndex(thisIndex), thisIndex]
		}.flatten
	}

	oppositePairsPoints { |theta = 0, phi = 0|
		^this.points.at(this.oppositePairsIndices(theta, phi))
	}

	oppositePairsPointsRev { |theta = 0, phi = 0|
		^this.points.at(this.oppositePairsIndicesRev(theta, phi))
	}

}