+ Score {

	recordNRTSync { |oscFilePath, outputFilePath, inputFilePath, sampleRate = 44100, headerFormat =
		"AIFF", sampleFormat = "int16", options, completionString="", duration = nil|
		var cmd;
		if(oscFilePath.isNil) {
			oscFilePath = PathName.tmp +/+ "temp_oscscore" ++ UniqueID.next;
		};
		this.writeOSCFile(oscFilePath, 0, duration);
		cmd = program + " -N" + oscFilePath.quote
			+ if(inputFilePath.notNil, { inputFilePath.quote }, { "_" })
			+ outputFilePath.quote
			+ sampleRate + headerFormat + sampleFormat +
			(options ? Score.options).asOptionsString
			+ completionString;
		cmd.systemCmd;
	}
}