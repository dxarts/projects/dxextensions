+ Scale {

	expandDegrees { |numNotes, startDegree|
		^this.tuning.expandDegrees(numNotes, startDegree, this)
	}

}